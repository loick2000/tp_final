namespace TP_Final_Service
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Activity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Activity()
        {
            Users = new HashSet<User>();
        }

        [Key]
        public int IDActivity { get; set; }

        public int Date_DateTimeStyles { get; set; }

        public int IDTypeActivity { get; set; }

        public bool Validated { get; set; }

        public int IDLocation { get; set; }

        public int? Group_IDGroup { get; set; }

        public virtual Group Group { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }
    }
}
