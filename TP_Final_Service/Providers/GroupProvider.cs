﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using TP_Final_Service.Interfaces;

namespace TP_Final_Service.Providers
{
    //#region Délégué de récupération d'informations
    //public delegate void RaiseInformationHandler(object sender, string information);
    //#endregion

    public class GroupProvider : IGroup
    {
        public void CreateGroup(Group group)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Groups.Add(group);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            finally
            {
                LogHelper.WriteToFile($"Création du groupe {group}", nameof(this.ToString));
            }
        }

        public void AddUserInGroup(string pseudo, Group group)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    var tempGroup = context.Groups.FirstOrDefault(g => g.IDGroup == group.IDGroup);
                    // ajouter du pseudo s'il n'existe pas déjà.
                    if (tempGroup.ListUsers.Where(p => p == pseudo).Count() == 0)
                        tempGroup.ListUsers.Add(pseudo);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            finally
            {
                LogHelper.WriteToFile("Ajout du user dans le group", nameof(this.ToString));
            }
        }

        public void DeleteUserInGroup(string pseudo, Group group)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    var tempGroup = context.Groups.FirstOrDefault(g => g.IDGroup == group.IDGroup);
                    if (tempGroup.ListUsers.Remove(pseudo))
                    {
                        context.SaveChanges();
                    }
                }
            }
            catch (DbUpdateException ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            finally
            {
                LogHelper.WriteToFile($"{pseudo} supprimé du groupe {group.NameGroup}", nameof(this.CreateGroup));
            }
        }

        public void DeleteGroup(Group group)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Groups.Remove(group);
                    context.SaveChanges();
                }
            }
            catch (DbUpdateException ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
            }
            finally
            {
                LogHelper.WriteToFile("Supression groupe", nameof(this.CreateGroup));
            }
        }

        public List<string> ListUserInGroup(Group group)
        {
            using (TPContext context = new TPContext())
            {
                try
                {
                    var listPseudo = context.Groups.First(g => g.IDGroup == group.IDGroup).ListUsers.ToList();
                    return listPseudo;

                }
                catch (DbUpdateException ex)
                {
                    LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
                    return null;
                }
                catch (Exception ex)
                {
                    LogHelper.WriteToFile(ex.Message, nameof(this.CreateGroup));
                    return null;
                }
                finally
                {
                    LogHelper.WriteToFile($"liste des user du groupe {group.NameGroup}", nameof(this.ToString));
                }
            }
        }
    }
}