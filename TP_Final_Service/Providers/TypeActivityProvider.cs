﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using TP_Final_Service.Interfaces;

namespace TP_Final_Service.Models
{
    public class TypeActivityProvider : ITypeActivity
    {
        public void CreatTypeActivity(TypeActivity typeActivity)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.TypeActivities.Add(typeActivity);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                 LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            finally
            {
                LogHelper.WriteToFile($"Création du type d'activité {typeActivity.Name}", nameof(this.ToString));
            }
        }

        public void ModifTypeActivity(TypeActivity typeActivity)
        {
             try
            {
                using (TPContext context = new TPContext())
                {
                    var tempTypeAct = context.TypeActivities.Where(a => a.IDTypeActivity == typeActivity.IDTypeActivity).FirstOrDefault();
                    tempTypeAct.MaxUser = typeActivity.MaxUser;
                    tempTypeAct.MinUser = typeActivity.MinUser;
                    tempTypeAct.Name = typeActivity.Name;
                    tempTypeAct.Duration = typeActivity.Duration;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                 LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            finally
            {
                LogHelper.WriteToFile($"Modification du type d'activité {typeActivity.Name}", nameof(this.ToString));
            }
        }
    }
}