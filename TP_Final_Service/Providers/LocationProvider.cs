using System;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using TP_Final_Service.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace TP_Final_Service.Providers
{
    public class LocationProvider : ILocation
    { 
        public LocationProvider()
        {
        }
    
        public void CreateLocation(Location location)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Locations.Add(location);

                    context.SaveChanges();
                    
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        public void ModifyLocation(Location location)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    var temp = context.Locations.Where(a => a.IDLocation == location.IDLocation).FirstOrDefault();
                    temp.StreetNumber = location.StreetNumber;
                    temp.Street = location.Street;
                    temp.PostalCode = location.PostalCode;
                    temp.City = location.City;
                    temp.Country = location.Country;

                    context.SaveChanges();
                    
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
            }
        }
        
        public void DeleteLocation(Location location)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Locations.Remove(location);

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
            }
        }

        
    }
}
