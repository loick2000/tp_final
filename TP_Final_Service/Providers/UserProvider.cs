﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TP_Final_Service.Providers
{
    public class UserProvider : IUser
    {
        public UserProvider()
        {
        }
        public List<Activity> ActivityByUser(User user)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    var query = context.Activities
                                .Where(t => t.Users.Equals(user));
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
                return null;
            }
            finally
            {
                LogHelper.WriteToFile("ActivityByUser", nameof(this.ToString));
            }
        }
        public void CreateUser(User user)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Users.Add(user);

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            finally
            {
                LogHelper.WriteToFile("passage par création du user", nameof(this.ToString));
            }
        }

        public void ModifyUser(User user)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    var temp = context.Users.Where(a => a.Pseudo == user.Pseudo).FirstOrDefault();
                    temp.Activities = user.Activities;
                    temp.IDLocation = user.IDLocation;

                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            finally
            {
                LogHelper.WriteToFile("passage par modification du user", nameof(this.ToString));
            }
        }

        public void DeleteUser(User user)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    context.Users.Remove(user);

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
            }
            finally
            {
                LogHelper.WriteToFile("passage par la supression du user", nameof(this.ToString));
            }
        }

        public void ValidationOnInvitation()
        {
            throw new NotImplementedException();
        }

        public bool VerifPassword(string pseudo, string password)
        {
            try
            {
                using (TPContext context = new TPContext())
                {
                    User user = context.Users.First(u => u.Pseudo == pseudo);
                    if (user.Password == password)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteToFile(ex.Message, nameof(this.ToString));
                return false;
            }
            finally
            {
                LogHelper.WriteToFile("vérification du password", nameof(this.ToString));

            }
        }
    }
}