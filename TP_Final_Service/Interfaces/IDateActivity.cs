﻿using System.ServiceModel;

namespace TP_Final_Service
{
    [ServiceContract]
    public interface IDateActivity
    {
        [OperationContract]
        void AddActivity();

        [OperationContract]
        void MoveActivity();

        [OperationContract]
        void AddUser();

        [OperationContract]
        void DeleteActivity();

        [OperationContract]
        void AddActivityLocation();

        [OperationContract]
        void ValidateActivityDate();

        [OperationContract]
        void CountRegisteredUser();

        [OperationContract]
        void ListActivityByDate();

        [OperationContract]
        void ListActivityByLocation();

        // s sur la class Location

        [OperationContract]
        void LocationCreate();

        [OperationContract]
        void DeleteLocation();

        [OperationContract]
        void ListLocation();
    }
}