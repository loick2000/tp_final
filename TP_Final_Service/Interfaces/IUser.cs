﻿using System.Collections.Generic;
using System.ServiceModel;

namespace TP_Final_Service
{
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        void CreateUser(User user);

        [OperationContract]
        bool VerifPassword(string pseudo,string password);

        [OperationContract]
        void DeleteUser(User user);

        [OperationContract]
        void ModifyUser(User user);

        [OperationContract]
        List<Activity> ActivityByUser(User user);

        [OperationContract]
        void ValidationOnInvitation();
    }
}