﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using TP_Final_Service.Models;

namespace TP_Final_Service.Interfaces
{
    [ServiceContract]
    public interface ITypeActivity
    {
        [OperationContract]
        void CreatTypeActivity(TypeActivity typeActivity);
        [OperationContract]
        void ModifTypeActivity(TypeActivity typeActivity);

    }
}
