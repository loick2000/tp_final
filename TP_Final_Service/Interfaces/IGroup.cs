﻿using System.Collections.Generic;
using System.ServiceModel;

namespace TP_Final_Service.Interfaces
{
    [ServiceContract]
    public interface IGroup
    {
        [OperationContract]
        void CreateGroup(Group group);

        [OperationContract]
        void AddUserInGroup(string pseudo, Group group);

        [OperationContract]
        void DeleteUserInGroup(string pseudo, Group group);

        [OperationContract]
        void DeleteGroup(Group group);

        [OperationContract]
        List<string> ListUserInGroup(Group group);
    }
}