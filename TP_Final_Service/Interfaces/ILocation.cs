using System.ServiceModel;

namespace TP_Final_Service.Interfaces
{
    [ServiceContract]
    public interface ILocation
    {
        [OperationContract]
        void CreateLocation(Location location);

        [OperationContract]
        void DeleteLocation(Location location);

        [OperationContract]
        void ModifyLocation(Location location);
    }
}