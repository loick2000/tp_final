﻿using System.Data.Entity;
using TP_Final_Service.Migrations;
using TP_Final_Service.Models;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace TP_Final_Service
{
    public partial class TPContext : DbContext
    {
        public TPContext() : base("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=database_tp_final;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
        {
        }

        public virtual DbSet<TypeActivity> TypeActivities { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Group> Groups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ////Généré automatiquement
            //modelBuilder.Entity<Activity>()
            //    .HasMany(e => e.Users)
            //    .WithMany(e => e.Activities)
            //    .Map(m => m.ToTable("UserActivities"));

            //modelBuilder.Entity<Group>()
            //    .HasMany(e => e.Activities)
            //    .WithOptional(e => e.Group)
            //    .HasForeignKey(e => e.Group_IDGroup);

            modelBuilder.Entity<User>()
                .Property(e => e.Pseudo);

            modelBuilder.Entity<Group>()
                .Property(e => e.IDGroup);

            modelBuilder.Entity<Location>()
                .Property(e => e.IDLocation);

            modelBuilder.Entity<Activity>()
                .Property(e => e.IDActivity);

            modelBuilder.Entity<TypeActivity>()
                .Property(e => e.IDTypeActivity);
        }
    }
}
