namespace TP_Final_Service.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TPContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        IDActivity = c.Int(nullable: false, identity: true),
                        Date_DateTimeStyles = c.Int(nullable: false),
                        IDTypeActivity = c.Int(nullable: false),
                        Validated = c.Boolean(nullable: false),
                        IDLocation = c.Int(nullable: false),
                        Group_IDGroup = c.Int(),
                    })
                .PrimaryKey(t => t.IDActivity)
                .ForeignKey("dbo.Groups", t => t.Group_IDGroup)
                .Index(t => t.Group_IDGroup);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Pseudo = c.String(nullable: false, maxLength: 128),
                        IDLocation = c.Int(nullable: false),
                        Validated = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Pseudo);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        IDGroup = c.Int(nullable: false, identity: true),
                        NameGroup = c.String(),
                    })
                .PrimaryKey(t => t.IDGroup);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        IDLocation = c.Int(nullable: false, identity: true),
                        StreetNumber = c.String(),
                        Street = c.String(maxLength: 100),
                        PostalCode = c.Int(nullable: false),
                        City = c.String(maxLength: 50),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.IDLocation);
            
            CreateTable(
                "dbo.TypeActivity",
                c => new
                    {
                        IDTypeActivity = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MinUser = c.Int(nullable: false),
                        MaxUser = c.Int(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        PseudoCreator = c.String(),
                    })
                .PrimaryKey(t => t.IDTypeActivity);
            
            CreateTable(
                "dbo.UserActivities",
                c => new
                    {
                        User_Pseudo = c.String(nullable: false, maxLength: 128),
                        Activity_IDActivity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Pseudo, t.Activity_IDActivity })
                .ForeignKey("dbo.Users", t => t.User_Pseudo, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.Activity_IDActivity, cascadeDelete: true)
                .Index(t => t.User_Pseudo)
                .Index(t => t.Activity_IDActivity);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities", "Group_IDGroup", "dbo.Groups");
            DropForeignKey("dbo.UserActivities", "Activity_IDActivity", "dbo.Activities");
            DropForeignKey("dbo.UserActivities", "User_Pseudo", "dbo.Users");
            DropIndex("dbo.UserActivities", new[] { "Activity_IDActivity" });
            DropIndex("dbo.UserActivities", new[] { "User_Pseudo" });
            DropIndex("dbo.Activities", new[] { "Group_IDGroup" });
            DropTable("dbo.UserActivities");
            DropTable("dbo.TypeActivity");
            DropTable("dbo.Locations");
            DropTable("dbo.Groups");
            DropTable("dbo.Users");
            DropTable("dbo.Activities");
        }
    }
}
