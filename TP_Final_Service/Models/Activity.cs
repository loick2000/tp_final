﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TP_Final_Service
{
    [DataContract]
    public class Activity
    {
        [DataMember]
        [Column(TypeName = "int")]
        [Key]
        public int IDActivity { get; set; }

        [DataMember]
        //[Column(TypeName = "timestamp")]
        public DateTimeFormat Date { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public int IDTypeActivity { get; set; }

        [DataMember]
        public bool Validated { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public int IDLocation { get; set; }

        [DataMember]
        public List<User> Users { get; set; }
    }
}