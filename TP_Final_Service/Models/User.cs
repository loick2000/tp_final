﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TP_Final_Service
{
    [DataContract]
    public partial class User
    {
        [DataMember]
        [Key]
        public string Pseudo { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public int IDLocation { get; set; }

        [DataMember]
        public int Validated { get; set; }

        [DataMember]
        public List<Activity> Activities { get; set; }
    }
}