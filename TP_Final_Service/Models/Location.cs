using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TP_Final_Service
{
    [DataContract]
    public class Location
    {
        [DataMember]
        [Column(TypeName = "int")]
        [Key]
        public int IDLocation { get; set; }

        [DataMember]
        public string StreetNumber { get; set; }

        [DataMember]
        [MaxLength(100)]
        public string Street { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public int PostalCode { get; set; }

        [DataMember]
        [MaxLength(50)]
        public string City { get; set; }

        [DataMember]
        public string Country { get; set; }
    }
}