﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TP_Final_Service
{
    [DataContract]
    public partial class Group
    {
        [DataMember]
        [Column(TypeName = "int")]
        [Key]
        public int IDGroup { get; set; }

        [DataMember]
        public string NameGroup { get; set; }

        [DataMember]
        public List<string> ListUsers { get; set; }
    }
}