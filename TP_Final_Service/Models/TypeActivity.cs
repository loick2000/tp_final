﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace TP_Final_Service.Models
{
    [DataContract]
    [Table("TypeActivity")]
    public partial class TypeActivity
    {
        [DataMember]
        [Column(TypeName = "int")]
        [Key]
        public Int32 IDTypeActivity { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public Int32 MinUser { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        public Int32 MaxUser { get; set; }
  
        [DataMember]
        //[Column(TypeName = "timestamp")]
        public TimeSpan Duration { get; set; }
  
        [DataMember]
        public string PseudoCreator { get; set; }
  }
}