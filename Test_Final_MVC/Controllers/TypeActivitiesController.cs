﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Test_Final_MVC.Models;

namespace Test_Final_MVC.Controllers
{
    public class TypeActivitiesController : Controller
    {
        private Test_Final_Entities db = new Test_Final_Entities();

        // GET: TypeActivities
        public ActionResult Index()
        {
            return View(db.TypeActivities.ToList());
        }

        // GET: TypeActivities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = db.TypeActivities.Find(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // GET: TypeActivities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TypeActivities/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDTypeActivity,Name,MinUser,MaxUser,Duration,PseudoCreator")] TypeActivity typeActivity)
        {
            if (ModelState.IsValid)
            {
                db.TypeActivities.Add(typeActivity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(typeActivity);
        }

        // GET: TypeActivities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = db.TypeActivities.Find(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // POST: TypeActivities/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDTypeActivity,Name,MinUser,MaxUser,Duration,PseudoCreator")] TypeActivity typeActivity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(typeActivity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(typeActivity);
        }

        // GET: TypeActivities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TypeActivity typeActivity = db.TypeActivities.Find(id);
            if (typeActivity == null)
            {
                return HttpNotFound();
            }
            return View(typeActivity);
        }

        // POST: TypeActivities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TypeActivity typeActivity = db.TypeActivities.Find(id);
            db.TypeActivities.Remove(typeActivity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
