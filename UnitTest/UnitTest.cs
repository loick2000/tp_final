﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    
    [TestClass]
    public class UnitTest1
    {
        // Test de la class DateActivity
        [TestMethod]
        public void TestAddActivity() { } // Methode centrale créant automatiquement les activités
        [TestMethod]
        public void TestMoveActivity() { }
        [TestMethod]
        public void TestAddUser() { }
        [TestMethod]
        public void TestDeleteActivity() { }
        [TestMethod]
        public void TestAddActivityLocation() { }
        [TestMethod]
        public void TestValidateActivityDate() { }
        [TestMethod]
        public void TestCountRegisteredUser() { }
        [TestMethod]
        public void TestListActivityByDate() { }
        [TestMethod]
        public void TestListActivityByLocation() { }
        // Tests sur la class Location
        [TestMethod]
        public void TestLocationCreate() { }
        [TestMethod]
        public void TestDeleteLocation() { }
        [TestMethod]
        public void TestListLocation() { }
        // Tests sur la class User
         [TestMethod]
        public void TestDeleteUser(){}
        [TestMethod]
        public void TestCreateUser() { }
        [TestMethod]
        public void TestModifyUser() { }
        [TestMethod]
        public void TestActivityByUser() { }
        [TestMethod]
        public void TestValidationOnInvitation() { }
    }
}

