﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using TP_Final_Service;
using TP_Final_Service.Interfaces;
using TP_Final_Service.Providers;

namespace UnitTest
{
    [TestClass]
    public class TestGroup
    {

        [TestMethod]
        public void TestCreateGroup()
        {
            var mock = new Mock<IGroup>();
            var group = new Group()
            {
                IDGroup = 1,
                NameGroup = "poker",
               ListUsers = { "j1", "j2" }
            };
            mock.Setup(r => r.CreateGroup(group))
                .Callback((int ID) => ID = group.IDGroup);
            Console.WriteLine(group.IDGroup);
            Assert.AreEqual(1 ,group.IDGroup );
        }

        [TestMethod]
        public void TestAddUserInGroup()
        {
            var mock = new Mock<IGroup>();
            var group = new Group()
            {
                IDGroup = 1,
                NameGroup = "poker",
               ListUsers = { "j1", "j2","j3" }
            };
            mock.Setup(r => r.AddUserInGroup("j5",group))
                .Callback((List<string> temp) => temp = group.ListUsers);
            Assert.AreEqual(4 ,group.ListUsers.Count );
            Assert.AreEqual("j5", group.ListUsers[3]);
        }

        [TestMethod]
        public void TestDeleteUserInGroup()
        {
        }

        [TestMethod]
        public void TestDeleteGroup()
        {
        }

        [TestMethod]
        public void TestListUserInGroup()
        {
            //var result = "";

            //var mockListUser = new Mock<GroupProvider>();
            //mockListUser.Setup(t => t.ListUserInGroup(user, group)).Returns(() => new List<User> { });
            //var resultList = mockListUser.Object.ListUserInGroup();
            //CollectionAssert.AreEquivalent(new List<int> { }, resultList);
        }

    }
}

