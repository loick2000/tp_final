﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TP_Final_Service.Models;

namespace UnitTest
{
    /// <summary>
    /// Description résumée pour TypActivity
    /// </summary>
    [TestClass]
    public class TestTypeActivity
    {
        public TestTypeActivity()
        {
            //
            // TODO: ajoutez ici la logique du constructeur
            //
        }

       #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestCreatTypeActivity()
        {
            var temp = new TypeActivity();
            temp.Duration = new TimeSpan(1, 0, 0, 0);
            temp.IDTypeActivity = 1;
            temp.MaxUser = 8;
            temp.MinUser = 2;
            temp.Name = "soirée poker";
            temp.PseudoCreator = "Loic";
            var requete = new TypeActivityProvider();
            requete.CreatTypeActivity(temp);


        }


        [TestMethod]
        public void TestModifTypeActivity()
        {
            //
            // TODO: ajoutez ici la logique du test
            //
        }
    }
}
