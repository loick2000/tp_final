﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TP_Final_Service;

namespace UnitTest
{
    
    [TestClass]
    public class TestUserProvider
    {
        [TestMethod]
        public void TestAddUser()
        {

        }

        [TestMethod]
        public void TestCreateUser()
        {
            var mockCreateUser = new Mock<IUser>();
            var user = new User()
            {
                Pseudo = "TestUnit",
                IDLocation = 1,
                Validated = 0,
                Activities = { }
            };
            mockCreateUser.Setup(r => r.CreateUser(user))
                .Callback((string ID) => ID = user.Pseudo);
            Assert.AreEqual(1, user.Pseudo);
        }

        [TestMethod]
        public void TestModifyUser() { }
        [TestMethod]
        public void TestActivityByUser() { }
         [TestMethod]
        public void TestDeleteUser(){}
        [TestMethod]
        public void TestValidationOnInvitation() { }
    }
}

